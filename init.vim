" Settings username
let settings_plugin = 'https://gitlab.com/flipxfx/vim-settings.git'

" Install/load vim-plug/vim-settings
let $EDITOR_PATH = expand('<sfile>:p:h')
let $SETTINGS_PATH = $EDITOR_PATH . '/plugged/vim-settings/config'
let $SETTINGS_FILE = $SETTINGS_PATH . '/settings.vim'
let plug_file = $EDITOR_PATH . '/autoload/plug.vim'
if empty(glob(plug_file))
  " Install plug
  echo 'Installing vim-plug...'
  silent exec '!curl -fLo ' . plug_file . ' --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  exec ':source ' . plug_file

  " Install settings
  echo "Installing vim-settings...\n"
  call plug#begin($EDITOR_PATH . '/plugged')
  Plug settings_plugin
  call plug#end()
  silent exec ':PlugInstall'

  " Install plugins in settings
  echo "Installing plugins found in vim-settings...\n"
  call plug#begin($EDITOR_PATH . '/plugged')
  exec ':source ' . $SETTINGS_FILE
  silent exec ':PlugInstall'
  exec ':q'
  exec ':source ' . $MYVIMRC
else
  " Load settings
  call plug#begin($EDITOR_PATH . '/plugged')
  Plug settings_plugin
  exec ':source ' . $SETTINGS_FILE
endif
