" Setting files to be sourced (press gf on file name line to open, or use ctrl-p)
let files=[
  \'./plugins.vim',
  \'./functions.vim',
  \'./plugins-settings.vim',
  \'./interface.vim',
  \'./general.vim',
  \'./maps.vim',
  \'./autocmds.vim'
\]

" Source each setting file
for file in files
  execute 'source' $SETTINGS_PATH . strpart(file, 1)
endfor
